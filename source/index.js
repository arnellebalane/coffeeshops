import Vue from 'vue';
import App from './components/App.vue';
import './stylesheets/index.css';
import './stylesheets/mapbox.css';

import mapboxgl from 'mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';

mapboxgl.accessToken = 'pk.eyJ1IjoiYXJuZWxsZWFibGFuZSIsImEiOiJjamliMXRzdHQwODN3M3ZtcG9zdWpxY2VxIn0._D89roKjHj8-jR8-LkJ3JA';

new Vue({
    el: '#app',
    render: h => h(App)
});
