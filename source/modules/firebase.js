import firebase from 'firebase';

firebase.initializeApp({
    apiKey: "AIzaSyCk-QN0jeT83LIwzk3Ru7VCl8SLGru19iQ",
    databaseURL: "https://coffeeshops-11144.firebaseio.com",
    projectId: "coffeeshops-11144"
});

export const database = firebase.database();
